" 기본 설정
syntax on                       " 자동 문법 강조
set encoding=utf-8              " 터미널(vim) 창에서 나타나는 인코딩
set fileencoding=utf-8          " 파일로 저장 시킬 때 사용하는 인코딩 

set sw=2                        " shift width : 자동 줄맞추기시 사용되는 크기
set sts=2                       " soft tab stop : tap 을 눌렀을 때 표시되는 크기
set smarttab                    " 라인 시작시, 자동으로 tap 넣어줌
set expandtab                   " insert 모드에서 tap 을 누르면, 스페이스를 입력
                                " 입력하는 스페이스 크기는 sts
                                " 백 스페이스로 sts 만큼 스페이스 삭제

set wildignore=*.o,*.obj,*.a,*.bak,*.exe,*~   " command line 에서 글자 완성시 해당 파일 제외

set cino=:0p0t0(0
set cindent                     " C 언어 자동 들여쓰기

set background=dark             " vim 의 배경색을 바꾸는 것이 아닌, 배경이 어둡다고 인지 시킴
set tagrelative                 " ?
set hlsearch                    " 찾는 결과를 하이라이트 표시
set showmatch                   " 매치되는 괄호의 반대쪽을 보여줌
set incsearch                   " 점진적으로 찾기
set nu                          " 줄 번호를 표시
set history=5000                " 편집한 5000 줄 기억
set ttymouse=xterm2             " ?
set mouse=a                     " 마우스를 사용할 수 있도록 설정 a 는 모든
set nocompatible                " Vim 디폴트 기능 사용
filetype off                    " ? 파일의 종류를 자동으로 인식 해제
hi Visual term=reverse cterm=reverse guibg=Grey

" F1 키를 누르면 줄 끝의 화이트 스페이스를 다 없앰
nnoremap <F1> :let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar><CR>

" F12 키를 누르면 vi 로 나눈 화면들이 동시에 오르락 내리락 함
nmap <F12> :windo set scrollbind!<CR>


let g:jsx_ext_required = 1

" VundleVim 설정
set runtimepath+=~/.vim/bundle/Vundle.vim
call vundle#begin()
  Plugin 'VundleVim/Vundle.vim'
  Plugin 'The-NERD-tree'
  Plugin 'taglist-plus'
  Plugin 'tpope/vim-fugitive'
  Plugin 'airblade/vim-gitgutter'
  Plugin 'vim-scripts/AutoComplPop'

  Plugin 'pangloss/vim-javascript'
  Plugin 'mxw/vim-jsx'
  Plugin 'prettier/vim-prettier'
call vundle#end()


let g:prettier#autoformat = 0
autocmd BufWritePre *.js,*.json,*.css,*.scss,*.less,*.graphql Prettier







filetype plugin indent on


" NERD Tree 설정
let NERDTreeWinPos = "left"
nmap <F7> :NERDTreeToggle<CR>
nmap <F8> :TlistToggle<CR>

" Tag list 설정
let Tlist_Inc_Winwidth = 0
let Tlist_Exit_OnlyWindow = 0
let Tlist_Auto_Open = 0
let Tlist_Use_Right_Window = 1

" ctags 설정
set tags+=./tags,../tags,../../tags,../../../tags,../../../../tags,../../../../../tags

" cscope 설정
"autocmd BufEnter *.[chly] call ScopeFindDBFile()
"nmap <C-\>s :cs find s <C-R>=expand("<cword>")<CR><CR>
"nmap <C-\>g :cs find g <C-R>=expand("<cword>")<CR><CR>
"nmap <C-\>c :cs find c <C-R>=expand("<cword>")<CR><CR>
"nmap <C-\>t :cs find t <C-R>=expand("<cword>")<CR><CR>
"nmap <C-\>e :cs find e <C-R>=expand("<cword>")<CR><CR>
"nmap <C-\>f :cs find f <C-R>=expand("<cfile>")<CR><CR>
"nmap <C-\>i :cs find i ^<C-R>=expand("<cfile>")<CR>$<CR>
"nmap <C-\>d :cs find d <C-R>=expand("<cword>")<CR><CR>
"
"function s:windowdir()
"  if winbufnr(0) == -1
"    return getcwd()
"  endif
"  return fnamemodify(bufname(winbufnr(0)), ':p:h')
"endfunc
"
"function s:Find_in_parent(fln,flsrt,flstp)
"  let here = a:flsrt
"  while ( strlen( here) > 0 )
"    if filereadable( here . "/" . a:fln )
"      return here
"    endif
"    let fr = match(here, "/[^/]*$")
"    if fr == -1
"      break
"    endif
"    let here = strpart(here, 0, fr)
"    if here == a:flstp
"      break
"    endif
"  endwhile
"  return "Nothing"
"endfunc
"
"function! ScopeFindDBFile()
"    if exists("b:csdbpath")
"      if cscope_connection(3, "out", b:csdbpath)
"        return
"        "it is already loaded. don't try to reload it.
"      endif
"    endif
"    let newcsdbpath = s:Find_in_parent("cscope.out",s:windowdir(),$HOME)
""    echo 'Found cscope.out at: ' . newcsdbpath
""    echo 'Windowdir: ' . s:windowdir()
"    if newcsdbpath != "Nothing"
"      let b:csdbpath = newcsdbpath
"      if !cscope_connection(3, "out", b:csdbpath)
"        let save_csvb = &csverb
"        set nocsverb
"        exe "cs add " . b:csdbpath . "/cscope.out " . b:csdbpath
"        set csverb
"        let &csverb = save_csvb
"      endif
"    endif
"endfunc
